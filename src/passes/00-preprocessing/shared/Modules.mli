module type S =
  sig
    val mk_module : string -> string -> string
  end
